import React from "react";
import { UserDeviceInfo } from "./UserDeviceInfo";

const App = () => {
  const deviceInfo = UserDeviceInfo();

  return (
    <div>
      <h1>Device Information</h1>
      <p>User Agent: {deviceInfo.userAgent}</p>
      <p>Screen Dimensions: {deviceInfo.screenDimensions}</p>
      <p>Device ID: {deviceInfo.deviceId}</p>
      <p>IP Address: {deviceInfo.ipAddress}</p>
      <p>Is iPad: {deviceInfo.isIPad ? "Yes" : "No"}</p>
      <p>Is Mobile: {deviceInfo.isMobile ? "Yes" : "No"}</p>
      <p>Is Tablet: {deviceInfo.isTablet ? "Yes" : "No"}</p>
      <p>Browser: {deviceInfo.browser}</p>
      <p>Operating System: {deviceInfo.operatingSystem}</p>
    </div>
  );
};

export default App;
