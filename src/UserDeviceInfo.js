import React, { useEffect, useState } from "react";
import {
  isMobile,
  isTablet,
  browserName,
  osName,
  isIPad13,
} from "react-device-detect"; // Import the necessary functions from react-device-detect
import FingerprintJS from '@fingerprintjs/fingerprintjs';
import { getFingerprint } from '@thumbmarkjs/thumbmarkjs'

export const UserDeviceInfo = () => {
  const [fingerprint, setFingerprint] = useState('');
  const [deviceInfo, setDeviceInfo] = useState({
    userAgent: navigator.userAgent,
    screenDimensions: `${window.screen.width}x${window.screen.height}`,
    deviceId: "",
    ipAddress: null,
    isIPad: isIPad13,
    isMobile: isMobile,
    isTablet: isTablet,
    browser: browserName,
    operatingSystem: osName,
  });

  useEffect(() => {
      const fpPromise = FingerprintJS.load();
      const getDeviceId = async () => {
      
        try {
          const fp = await fpPromise;
          const result = await fp.get({ extendedResult: true });
          console.log(" Reult : ", result);
          const visitorId = result.visitorId;
          console.log('Visitor ID:', visitorId);
          return visitorId;
        } catch (error) {
          console.error('Error obtaining fingerprint:', error);
          return 'Unavailable';
        }
      };
      const fetchIpAddress = async () => {
        try {
          const response = await fetch("https://api.ipify.org/?format=json");
          const data = await response.json();
          getFingerprint()
          .then((result) => {
            setFingerprint(result);
            console.log("result -- ", result);
            const updatedDeviceInfo = {
              ...deviceInfo,
              deviceId: result,
              ipAddress: data.ip,
            };
            console.log("updatedDeviceInfo -- ", updatedDeviceInfo);
            setDeviceInfo(updatedDeviceInfo);
          })
          .catch((error) => {
            console.error('Error getting fingerprint:', error);
          })
         
         
        } catch (error) {
          console.error("Failed to fetch IP address:", error);
          const updatedDeviceInfo = {
            ...deviceInfo,
            ipAddress: "Unavailable",
          };
          updatedDeviceInfo.deviceId = getDeviceId(updatedDeviceInfo);
          setDeviceInfo(updatedDeviceInfo);
        }
      };
  
      fetchIpAddress();
  }, []);

  return deviceInfo;
};
